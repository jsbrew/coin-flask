// Next.js demands to define if using Client component or Server Component
// If not written, getting error when using useRef, useEffect etc. 
'use client';

import { useEffect, useRef } from 'react';
import styles from './nav.module.css';
import AppLogo from '../logo/logo';

// Function for observing when to set styles for navbar.
function observeNav(navRef: HTMLElement, watcherRef: HTMLDivElement) {
    const navbar: HTMLElement = navRef;
    const watcherElement: HTMLDivElement = watcherRef;

    const observer = new IntersectionObserver(entries => {
        entries.forEach(entry => {
            navbar.classList.toggle('transparent', !entry.isIntersecting);
        });
    }, {
        rootMargin: `-${navbar.offsetHeight}px`,
    });
    observer.observe(watcherElement);
}

function Nav() {
    // Using useRef to pick html(nav) element while null being initial value.
    const navRef = useRef<HTMLElement>(null);
    // Observing this element to set styles for navbar
    const watcherRef = useRef<HTMLDivElement>(null);
    
    // Calling observeNav with useEffect once component is rendered.
     useEffect(() => {
        // Making sure that navRef is not null to prevent errors/warnings.
        if (navRef.current !== null && watcherRef.current !== null) {
            observeNav(navRef.current, watcherRef.current)
        }
     }, []);

    return (
        <>
        <nav className={`${styles.nav}`} ref={navRef}>
            <div className={`container ${styles.nav__container}`}>
                <div className={`${styles.nav__logo}`}>
                    <AppLogo />
                </div>
            </div>     
        </nav>
        <div ref={watcherRef}></div>
        </>
    );
}

export default Nav;