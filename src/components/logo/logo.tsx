import styles from './logo.module.css';

function AppLogo() {
    return (
        <div className={`${styles.logo__app}`}>
            <h2>C<span><i className="fa-brands fa-bitcoin fa-xs"></i></span>inFlask</h2>
        </div>
    )
}

export default AppLogo;