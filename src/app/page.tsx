/*
import Image from "next/image";
import styles from "./page.module.css";
*/
import Footer from "@/components/footer/footer";
import Hero from "@/components/hero/hero";
import InfoBar from "@/components/infobar/infobar";
import Nav from "@/components/nav/nav";
import Showcase from "@/components/showcase/showcase";

function Home() {
  return (
      <main>
        <InfoBar />
        <Nav />
        <Hero />
        <Showcase />
        <Footer />
      </main>
  );
}

export default Home;