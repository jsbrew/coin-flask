import { useEffect, useRef, useState } from 'react';
import styles from './select.module.css';

type SelectOption = {
    label: string;
    value: any;
    symbol: string;
    price: any;
}

type SelectProps = {
    options: SelectOption[];
    value?: SelectOption;
    onChange: (value: SelectOption | undefined) => void;
}

function Select({ value, onChange, options }: SelectProps) {
    const [isOpen, setIsOpen] = useState(false);
    const [highlightedIndex, setHighlightedIndex] = useState(0);
    const containerRef = useRef<HTMLDivElement>(null);

    function clearOptions() {
        onChange(undefined);
    }

    function selectOption(option: SelectOption) {
        if (option !== value) onChange(option);
    }

    function isOptionSelected(option: SelectOption) {
        return option === value;
    }

    useEffect(() => {
        if (isOpen) setHighlightedIndex(0);
    }, [isOpen]);

    useEffect(() => {
        const handler = (e: KeyboardEvent) => {
            if (e.target != containerRef.current) return;
            switch(e.code) {
            case 'Enter':
            case 'Space':
                setIsOpen(prev => !prev);
                if (isOpen) selectOption(options[highlightedIndex]);
                break;
            case 'ArrowUp':
            case 'ArrowDown': {
                if (!isOpen) {
                    setIsOpen(true);
                    break;
                }

                const newValue = highlightedIndex + (e.code === 'ArrowDown' ? 1 : -1);
                if (newValue >= 0 && newValue < options.length) {
                    setHighlightedIndex(newValue);
                }
                break;
            }
            case 'Escape':
                setIsOpen(false);
                break;
            }
        }

        containerRef.current?.addEventListener('keydown', handler);

        return () => {
        containerRef.current?.removeEventListener('keydown', handler);
        }
    }, [isOpen, highlightedIndex, options]);

    return (
        <div 
            ref={containerRef}
            className={`${styles.select}`} 
            onClick={() => setIsOpen(prev => !prev)} 
            onBlur={() => setIsOpen(false)} 
            tabIndex={0}
        >
            <span className={`${styles.select__value}`}>{value?.label}</span>
            <button 
                className={`${styles.select__clear}`}
                onClick={e => {
                    e.preventDefault();
                    e.stopPropagation();
                    clearOptions();
                }}
            >
                &times;
            </button>
            <div className={`${styles.select__divider}`}></div>
            <div className={`${styles.select__caret}`}></div>
            <ul className={`${styles.select__options} ${isOpen ? styles.show : ''}`}>
                {options.map((option, index) => (
                    <li 
                        key={option.value} 
                        className={`${styles.select__option} ${isOptionSelected(option) ? styles.selected : ''} ${index === highlightedIndex ? styles.highlighted : ''}`}
                        onMouseEnter={() => setHighlightedIndex(index)}
                        onClick={e => {
                            e.stopPropagation();
                            selectOption(option);
                            setIsOpen(false);
                        }}
                    >
                        {option.label}
                    </li>
                ))}
            </ul>
        </div>
    )
}

export default Select;