import styles from './infobar.module.css';

function InfoBar() {
    return (
        <section className={`section ${styles.info}`}>
            <div className={`container ${styles.info__container}`}>
                <div className={`${styles.info__heading}`}>
                    <i className="fa-solid fa-screwdriver-wrench"></i>
                    <p><b>Status:</b> Under Construction!</p>
                </div>
            </div>
        </section>
    )
}

export default InfoBar;