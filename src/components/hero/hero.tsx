import styles from './hero.module.css';

function Hero() {
    return (
        <section className={`section ${styles.hero}`}>
            <div className={`container ${styles.hero__container}`}>
                <div className={`${styles.hero__heading}`}>
                
                </div>
            </div>
        </section>
    );
}

export default Hero;