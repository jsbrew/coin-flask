// Next.js demands to define if using Client component or Server Component
// If not written, getting error when using useRef, useEffect etc. 
'use client';

import { useEffect, useRef } from 'react';
import styles from './footer.module.css';
import AppLogo from '../logo/logo';
import Contacts from '../contacts/contacts';

function Footer() {
    // Getting span element.
    const yearRef = useRef<HTMLSpanElement>(null);

    // After render setting current year to footer span.
    useEffect(() => {
        if (yearRef.current) {
            yearRef.current.textContent = String(new Date().getFullYear());
        }
    }, [])

    return (
        <section className={`section ${styles.footer}`}>
            <div className={`container ${styles.footer__container}`}>
                <div className={`${styles.footer__top}`}>
                    <AppLogo />
                </div>
                <div className={`${styles.footer__middle}`}>
                    <a href="https://juseport.netlify.app" target="_blank" rel="noopener noreferrer">
                        <img className={styles.footer__logo} src="/assets/logos/green_logo.png" alt="logo" />
                        <h5>HAND CRAFTED BY DIGITAL ALCHEMIST</h5>
                    </a>
                </div>
                <div className={`${styles.footer__bottom}`}>
                    <Contacts />
                    <div className={`${styles.footer__cp}`}>
                        <small>
                            Copyright @ <span ref={yearRef}>2024</span> | JS All Rights Reserved
                        </small>
                    </div>
                </div>
            </div>
        </section>
    );
}

export default Footer;