import styles from './contacts.module.css';

function Contacts() {
    return (
        <div className={`${styles.contacts}`}>
            <div className={`${styles.contacts__item}`}>
                <a href="https://linkedin.com/in/j-seppala" target="_blank" rel="noopener noreferrer" aria-label="LinkedIn link">
                    <i className="fa-brands fa-linkedin fa-2xl"></i>
                </a>
            </div>
            <div className={`${styles.contacts__item}`}>
                <a href="https://gitlab.com/jsbrew" target="_blank" rel="noopener noreferrer" aria-label="Gitlab link">
                    <i className="fa-brands fa-square-gitlab fa-2xl"></i>
                </a>
            </div>
            <div className={`${styles.contacts__item}`}>
                <a href="mailto:jsdev@mm.st" target="_blank" rel="noopener noreferrer" aria-label="Email link">
                    <i className="fa-solid fa-envelope fa-2xl"></i>
                </a>
            </div>
        </div>
    )
}

export default Contacts;