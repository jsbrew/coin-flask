'use client';

import { FormEvent, useEffect, useRef, useState } from 'react';
import styles from './dialog.module.css';
import Select from '../select/select';
import options from '../../data/coins.json';
import AppLogo from '../logo/logo';

class Coin {
    name: string;
    symbol: string;

    constructor(name: string, symbol: string) {
        this.name = name;
        this.symbol = symbol;
    }
}

type DialogProps = {
    status?: boolean;
    onCloseModal: () => void;
    showCoinData: (query: string, coinName: string) => void;
}

function DialogModal({ status, onCloseModal, showCoinData }: DialogProps) {
    const modalRef = useRef<HTMLDialogElement>(null);
    const [value, setValue] = useState<typeof options[0] | undefined> (options[0]);
    const [loading, setLoading] = useState(false);

    function closeModal() {
        if (modalRef.current) {
            modalRef.current.close();
            modalRef.current.classList.remove(styles.transmute);
            document.body.classList.remove('modalOpen');
            status = false;
            onCloseModal();
        }
    }

    async function selectCoin (e: FormEvent) {
        e.preventDefault();
        if (value && !loading) {
            setLoading(true);
            try {
                const newCoin: Coin = new Coin(value.label as string, value.symbol as string);
                await showCoinData(newCoin.symbol, value.label);
                closeModal();
            }
            catch (error) {
                console.error('Error fetching coin data:', error);
            }
            finally {
                setLoading(false);
            }
        }
    }

    useEffect(() => {
        if (status && modalRef.current) {
            modalRef.current.showModal();
            modalRef.current.classList.add(styles.transmute);
            document.body.classList.add('modalOpen');
        }
    }, [status]);

    useEffect(() => {
        /* Reset modal when cllicking Esc */
        const handler = (e: KeyboardEvent) => {
            if (e.code === 'Escape') closeModal();
        };
        document.addEventListener('keydown', handler);
        // Return a cleanup function when component is unmounted/depencies change. Prevents memory leaks and unnecessary event listeners.
        return () => {
            document.removeEventListener('keydown', handler);
        };
    }, [closeModal])

    return (
        <dialog className={`dialog ${styles.dialog}`} ref={modalRef}>
            <div className={`${styles.dialog__container}`}>
                <h2><span className={`highlight__obj`}>Select</span> and <span className={`highlight__obj`}>Transmute</span> Coin</h2>
                <form onSubmit={(e) => selectCoin(e)}>
                    <label>Select a Coin:</label>
                    <Select 
                        options={options}
                        value={value}
                        onChange={o => setValue(o)}
                    />

                    <button 
                        className={`${styles.dialog__btn} ${styles.dialog__submit}`} 
                        type="submit"
                    >
                        <i className={`fa-solid fa-wand-magic-sparkles highlight__obj`}></i>
                        <b>Transmute Coin</b>
                    </button>
                    <button 
                        className={`${styles.dialog__btn}`} 
                        type="button"
                        onClick={() => closeModal()}
                    >
                        <i className={`fa-solid fa-flask highlight__obj`}></i>
                        <b>Close the Flask</b>
                    </button>
                </form>
                <div className={`${styles.dialog__footer}`}>
                    <AppLogo />
                </div>
            </div>
        </dialog>
    )
}

export default DialogModal;