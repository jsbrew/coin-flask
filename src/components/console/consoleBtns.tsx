'use client';

import { useEffect, useRef, useState } from 'react';
import styles from './consoleBtns.module.css';
import DialogModal from '../dialog/dialog';

type ConsoleProps = {
    showCoinData: (query: string, coinName: string) => void;
    updatePrices: () => void;
}

function ConsoleBtns({ showCoinData, updatePrices } : ConsoleProps) {
    const menuRef = useRef<HTMLButtonElement>(null);
    const [menuOpen, setMenuOpen] = useState(false);
    const [modalOpen, setModalOpen] = useState(false);

    // Function handeling state of the menu (open/true or closed/false).
    function handleMenuClick() {
        setMenuOpen(prev => !prev);
    };

    /* Function for modal reset */
    function resetModal() {
        setModalOpen(false);
    }

    useEffect(() => {
        /* Menu button feature */
        if (menuRef.current) {
            // Adding event listener to the menu button.
            menuRef.current.addEventListener('click', handleMenuClick);
            // Return a cleanup function when component is unmounted/depencies change. Prevents memory leaks and unnecessary event listeners.
            return () => {
                menuRef.current?.removeEventListener('click', handleMenuClick);
            };
        }
    }, []);

    return (
        <div className={`${styles.console}`}>
            <div className={`${styles.console__container}`}>
                <ul className={`${styles.console__list}`}>
                    <li className={`${styles.console__item} ${menuOpen ? styles.openItemFour : styles.closeItemFour}`}>
                        <button className={`${styles.console__btn}`} aria-label="">
                            <i className="fa-solid fa-chevron-right fa-2xl"></i>
                        </button>
                    </li>
                    <li className={`${styles.console__item} ${menuOpen ? styles.openItemThree : styles.closeItemThree}`}>
                        <button className={`${styles.console__btn}`} aria-label="">
                            <i className="fa-solid fa-chevron-left fa-2xl"></i>
                        </button>
                    </li>
                    <li className={`${styles.console__item} ${menuOpen ? styles.openItemTwo : styles.closeItemTwo}`}>
                        <button className={`${styles.console__btn}`} onClick={() => updatePrices()} aria-label="Update coins">
                            <i className="fa-solid fa-rotate fa-2xl"></i>
                        </button>
                    </li>
                    <li className={`${styles.console__item} ${menuOpen ? styles.openItemOne : styles.closeItemOne}`}>
                        <button className={`${styles.console__btn}`} onClick={() => setModalOpen(true)}  aria-label="Add coin modal">
                            <i className="fa-solid fa-plus fa-2xl"></i>
                        </button>
                    </li>
                    <li className={`${styles.console__item} ${styles.console__menu}`}>
                        <button className={`${styles.console__btn}`} ref={menuRef}  aria-label="Open menu">
                            <i className={`fa-solid fa-flask fa-2xl ${menuOpen ? styles.rotateIcon : styles.staticIcon}`}></i>
                        </button> 
                    </li>
                </ul>
            </div>
            <DialogModal status={modalOpen} onCloseModal={resetModal} showCoinData={showCoinData} />
        </div>
    )
}

export default ConsoleBtns;