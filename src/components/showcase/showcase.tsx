'use client';

import { useEffect, useState } from 'react';
import ConsoleBtns from '../console/consoleBtns';
import styles from './showcase.module.css';

function Showcase() {
    const [isClient, setIsClient] = useState(false);
    const [allCoins, setAllCoins] = useState<object[]>(() => {
    let storedCoins;

    if (typeof window !== 'undefined') {
        storedCoins = localStorage.getItem('allCoins');
    }

    return storedCoins ? JSON.parse(storedCoins) : [];
    });

    async function updateAllCoins() {
        const priceUrl = 'https://api.binance.com/api/v3/ticker/price?symbol=';
        const priceChangeUrl = 'https://api.binance.com/api/v3/ticker/24hr?symbol=';
        const currentCoins = JSON.parse(localStorage.getItem('allCoins') ?? '[]');

        if (currentCoins !== null && currentCoins.length > 0) {
            const updatedCoins = await Promise.all(currentCoins.map(async (coin: any) => {
                const newPriceData = await fetchCoinData(`${priceUrl}${coin.query}`);
                const newPriceChangeData = await fetchCoinData(`${priceChangeUrl}${coin.query}`);
                const newPrice = newPriceData.price;
                const newPriceChange = newPriceChangeData.priceChange;
                const newPriceChangePercent = newPriceChangeData.priceChangePercent;
                // Update price value and return updated coin object
                return { ...coin, price: newPrice, change24: newPriceChange, changePercent24: newPriceChangePercent }; 
            }));
            setAllCoins(updatedCoins); // Update state with the new array of coins
        }
    }

    /* Function to handle fetched coin. Creates new object and saves it in the localStorage, if not already... */
    async function handleCoin(newCoin: object) {
        const coinToFind = (newCoin as { name: string }).name;
        const coinIsIncluded = allCoins.some(coin => (coin as { name: string }).name === coinToFind);

        if (!coinIsIncluded) {
            setAllCoins(prevCoins => {
                const updatedCoins = [...prevCoins, newCoin];
                if (typeof window !== 'undefined') {
                    localStorage.setItem('allCoins', JSON.stringify(updatedCoins));
                }
                return updatedCoins;
            });
        }
    }

    /* Reusable function for fetching data */
    async function fetchCoinData(url: string) {
        try {
            const res = await fetch(url);

            if (!res.ok) {
                throw new Error(`HTTP error! Status: ${res.status}`);
            }
            const data = await res.json();
            return data;
        }
        catch (error) {
            console.log('An error occured:', error);
        }
    }

    /* Main function, which controls the minor functions above */
    async function showCoinData(query: string, coinName: string) {
        /* Base URL for fetching ticker prices from Binance API */
        const price = 'https://api.binance.com/api/v3/ticker/price?symbol=';
        /* Base URL for fetching ticker price changes from Binance API */
        const difference = 'https://api.binance.com/api/v3/ticker/24hr?symbol=';
        /* Complete URLs with the desired queries */
        const priceUrl = `${price}${query}`;
        const priceChangeUrl = `${difference}${query}`;

        try {
            /* Fetching price data */
            const priceData = await fetchCoinData (priceUrl);
            const coinPrice = priceData.price;
            const coinQuery = priceData.symbol;
            const indexOfEur = priceData.symbol.indexOf('EUR');
            const coinCode = priceData.symbol.substring(0, indexOfEur);

            /* Fetching price change (24h) data */
            const priceChange = await fetchCoinData(priceChangeUrl);
            const priceChangePercent = priceChange.priceChangePercent;
            const coinPriceChange = priceChange.priceChange;
            /*console.log(priceChange);*/

            /* Sending all data to handeling function to create a coin */
            handleCoin({name: coinName, price: coinPrice, change24: coinPriceChange, changePercent24: priceChangePercent, code: coinCode, query: coinQuery})
        }
        catch (error) {
            console.error('An error occurred:', error);
        }
    }

    function removeCoin(name: string) {
        if (allCoins.length > 0) {
            setAllCoins(prevCoins => prevCoins.filter(coin => (coin as any).name !== name));
        }
    }

    useEffect(() => {
        setIsClient(true);
        updateAllCoins();
        const intervalId = setInterval(updateAllCoins, 60000);
        return () => clearInterval(intervalId);
    }, []);

    useEffect(() => {
        localStorage.setItem('allCoins', JSON.stringify(allCoins));
    }, [allCoins]);

    return (
        <section className={`section ${styles.showcase}`}>
            <div className={`container ${styles.showcase__container}`}>
                <div className={`${styles.cards}`}>
                    {isClient && allCoins.map((coin, index) => (
                        <div key={index} className={`${styles.card__wrapper}`}>
                            <div className={`${styles.card}`}>
                                <div className={`${styles.card__heading}`}>
                                    <div className={`${styles.card__title}`}>
                                        <i className={`fa-solid fa-cubes`}></i>
                                        <h4>{(coin as any).name}</h4>
                                    </div>
                                    <span className={`${styles.card__tag}`}><small>{(coin as any).code}</small></span>
                                </div>
                                <div className={`${styles.card__body}`}>
                                    <div className={`${styles.card__currency}`}>
                                        <i className={`fa-solid fa-coins`}></i>
                                        <span><small>{(coin as any).price} €</small></span>
                                    </div>
                                    <div className={`${styles.card__currency}`}>
                                        <i className={`fa-solid fa-scale-unbalanced`}></i>
                                        <span><small>{(coin as any).change24} €</small></span>
                                    </div>
                                    <div className={`${styles.card__currency}`}>
                                        <i className={`fa-solid fa-chart-simple`}></i>
                                        <span><small>{(coin as any).changePercent24} %</small></span>
                                    </div>
                                </div>
                                <div className={`${styles.card__footer}`}>
                                    <button onClick={() => removeCoin((coin as any).name)}><i className={`fa-solid fa-ban`}></i> Remove</button>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            
                <div className={`${styles.showcase__controls}`}>
                    <ConsoleBtns showCoinData={showCoinData} updatePrices={updateAllCoins} />
                </div>
                        
            </div>

        </section>
    )
}

export default Showcase;